﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("dxf_instrument_profile_live_sample")]
[assembly: AssemblyDescription("dxFeed instrument profile live sample")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("DevExperts")]
[assembly: AssemblyProduct("dxf_instrument_profile_live_sample")]
[assembly: AssemblyCopyright("Copyright © 2014-2016 Devexperts LLC")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("93a06e32-50a6-4071-883b-34b5c3edae55")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("5.7.0.0")]
[assembly: AssemblyFileVersion("5.7.0.0")]
